// PARTE DO DASHBOARD (VERO O QUE FAZ, ESTUDAR O CODIGO)
$(function () {
    "use strict";

    $(".preloader").fadeOut();
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").on('click', function () {
        $("#main-wrapper").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
    });
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function () {
        $(".app-search").toggle(200);
        $(".app-search input").focus();
    });

    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body, .page-wrapper").trigger("resize");
    $(".page-wrapper").delay(20).show();

    //****************************
    /* This is for the mini-sidebar if width is less then 1170*/
    //**************************** 
    var setsidebartype = function () {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        if (width < 1170) {
            $("#main-wrapper").attr("data-sidebartype", "mini-sidebar");
        } else {
            $("#main-wrapper").attr("data-sidebartype", "full");
        }
    };
    $(window).ready(setsidebartype);
    $(window).on("resize", setsidebartype);
});
// FIM DA PARTE DO DASHBOARD

// PARTE QUE FAZ APARECER OS BALOESZINHOS PRETOS NOS LINKS
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
// FIM DOS BALOESZINHOS

// SCRIPT Q POSICIONA OS ALERTAS C/ AS MENSAGENS (RETORNO) DAS CONTROLLERS 
$(function () {
    var jElement = $('.alert');

    $(document).ready(function () {
        if ($(this).scrollTop() >= 0) {
            jElement.css({
                'position': 'fixed',
                'bottom': '5%',
                'right': '3%'
            });
        }
    });
});

// SCRIPT QUE COLOCA AS MASCARAS NOS INPUTS
$(document).ready(function () {
    //specifying options 
    $("#celular1").inputmask({
        "mask": "(99) 99999-9999"
    });
    $("#celular2").inputmask({
        "mask": "(99) 99999-9999"
    });
    $("#residencial").inputmask({
        "mask": "(99) 9999-9999"
    });
    $("#cep").inputmask({
        "mask": "99999-999"
    });
    $("#uf").inputmask({
        "mask": "AA"
    });   
    /**$("#custo").inputmask({
        "mask": "9{1,4},99"
    });*/
});
// FIM DO SCRIPT QUE COLOCA AS MASCARAS NOS INPUTS

// SCRIPT QUE USA A API DE BUSCA DE CEP VIA AJAX
$(document).ready(function () {
    $('#cep').blur(function () {
        $.ajax({
            url: 'http://cep.republicavirtual.com.br/web_cep.php?cep=' + $('#cep').val() + '&formato=json',
            dataType: 'json',
            success: function (data) {
                if (data.resultado === "1") {
                    $('#rua').val(data.logradouro);
                    $('#cidade').val(data.cidade);
                    $('#bairro').val(data.bairro);
                    $('#uf').val(data.uf);
                    $('#numero').focus();
                }
            }
        });
    });
});
// FIM DO SCRIPT DE BUSCA DE CEP
