      
         @foreach($clientes as $c)
         <tr>
            <td><a href="/admin/clientes/perfil/{{$c->id}}" class="text-secondary">
               <img src="/storage/{{$c->foto}}" class="rounded-circle" width="40" height="40" />
               &nbsp;<strong>{{$c->name}}</strong></a>
            </td>
            <td>{{$c->email}}</td>
            <td>
               @if($c->contato->rua != "—")
               {{$c->contato->rua}}, n° {{$c->contato->numero}}
               @else
               —
               @endif                                      
               @if($c->contato->complemento != "—")
               — {{$c->contato->complemento}}
               @else                                            
               @endif
            </td>
            <td class="text-center">{{$c->contato->bairro}}</td>
            <td class="text-center">{{$c->contato->celular1}}</td>
            <td class="text-center">            
               <a href="/admin/clientes/editar/{{$c->id}}" class="text-info" 
                  title="Editar"><i class="mdi mdi-pencil"></i></a>&nbsp;                                          
               <a href="/admin/desativar/{{$c->id}}" class="text-danger" title="Desativar"><i class="mdi mdi-block-helper"></i></a>                                           
            </td>
         </tr>
         @endforeach  
     
  