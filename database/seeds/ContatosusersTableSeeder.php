<?php

use Illuminate\Database\Seeder;

class ContatosusersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contatosusers')->insert([
            'id' => 1, 'cep' => '96025-230',
            'rua' => 'Manoel Caetano da Silva',
            'numero' => '156', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98132-9000',
            'user_id' => 1, 'created_at' => '2018-10-31 23:34:23',
            'updated_at' => '2018-10-31 23:35:46'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 2, 'cep' => '96025-260',
            'rua' => 'Nossa Senhora da Aparecida',
            'numero' => '240', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 99123-4566',
            'user_id' => 2, 'created_at' => '2018-11-02 00:10:35',
            'updated_at' => '2018-11-02 00:11:53'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 3, 'cep' => '96015-310',
            'rua' => 'Barão da Conceição',
            'numero' => '308', 'bairro' => 'Centro',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 99934-9876', 'celular2' => '(53) 99122-9879',
            'residencial' => '(53) 3284-8745',
            'instagram' => 'https://www.instagram.com/ritacadillac/',
            'user_id' => 3, 'created_at' => '2018-11-02 00:31:09',
            'updated_at' => '2018-11-02 15:14:47'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 4, 'cep' => '96020-480',
            'rua' => 'Marcílio Dias',
            'numero' => '1295', 'bairro' => 'Centro',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98445-3212', 'celular2' => '(53) 99122-9879',
            'residencial' => '(53) 3284-8745',
            'twitter' => 'https://twitter.com/tatawerneck',
            'instagram' => 'https://www.instagram.com/tatawerneck/',
            'user_id' => 4, 'created_at' => '2018-11-02 15:25:25',
            'updated_at' => '2018-11-02 15:32:16'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 5, 'cep' => '96020-380',
            'rua' => 'Santos Dumont',
            'numero' => '567', 'bairro' => 'Centro',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98413-1200',
            'residencial' => '(53) 3284-8745',
            'facebook' => 'https://pt-br.facebook.com/MarcosMionOficial/',
            'twitter' => 'https://twitter.com/marcosmion',
            'instagram' => 'https://www.instagram.com/marcosmion/',
            'user_id' => 5, 'created_at' => '2018-11-02 19:41:34',
            'updated_at' => '2018-11-02 19:42:55'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 6, 'cep' => '96015-700',
            'rua' => 'Doutor Cassiano',
            'numero' => '398', 'bairro' => 'Centro',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'complemento' => 'Casa 3',
            'celular1' => '(53) 98423-4567',
            'facebook' => 'https://pt-br.facebook.com/rafinhabastos/',
            'twitter' => 'https://twitter.com/rafinhabastos',
            'instagram' => 'https://www.instagram.com/rafinhabastos/',
            'user_id' => 6, 'created_at' => '2018-11-02 19:52:14',
            'updated_at' => '2018-11-02 20:18:22'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 7, 'cep' => '96015-190',
            'rua' => 'Major Cícero de Góes Monteiro',
            'numero' => '295', 'bairro' => 'Centro',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98134-1234',
            'user_id' => 7, 'created_at' => '2018-11-02 20:27:31',
            'updated_at' => '2018-11-02 20:29:06'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 8, 'cep' => '96030-330',
            'rua' => 'Doutor Frederico Bastos',
            'numero' => '1005', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98465-1252',
            'user_id' => 8, 'created_at' => '2018-11-02 21:26:52',
            'updated_at' => '2018-11-02 21:29:41'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 9, 'cep' => '96015-730',
            'rua' => 'Voluntários da Pátria',
            'numero' => '555', 'bairro' => 'Centro',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98122-3455',
            'user_id' => 9, 'created_at' => '2018-11-02 21:34:36',
            'updated_at' => '2018-11-02 21:36:05'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 10, 'cep' => '96025-070',
            'rua' => 'Coronel Onofre Pires',
            'numero' => '111', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98122-4576',
            'user_id' => 10, 'created_at' => '2018-11-02 22:04:01',
            'updated_at' => '2018-11-02 22:05:11'
        ]);
        DB::table('contatosusers')->insert([
            'id' => 11, 'cep' => '96020-200',
            'rua' => 'Manoelito de Ornellas',
            'numero' => '10', 'bairro' => 'Três Vendas',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 99121-1234',
            'user_id' => 11, 
            'created_at' => '2018-10-12 22:04:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 12, 'cep' => '96020-210',
            'rua' => 'Márcio Dias',
            'numero' => '1023', 'bairro' => 'Três Vendas',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 99941-5678', 
            'user_id' => 12, 
            'created_at' => '2018-10-18 22:04:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 13, 'cep' => '96050-220',
            'rua' => 'Maurílio Martins Villar',
            'numero' => '678', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98432-3333', 
            'user_id' => 13, 
            'created_at' => '2018-10-22 22:04:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 14, 'cep' => '96040-200',
            'rua' => 'Adalberto Guerra Duval',
            'numero' => '564', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 99909-2345', 
            'user_id' => 14, 
            'created_at' => '2018-11-05 22:04:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 15, 'cep' => '96030-500',
            'rua' => 'Quatro (Lot Res Visc da Graça)',
            'numero' => '1', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98441-5608', 
            'user_id' => 15, 
            'created_at' => '2018-12-01 22:18:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 16, 'cep' => '96040-280',
            'rua' => 'Doutor Mário Totta',
            'numero' => '554', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98113-3244', 
            'user_id' => 16, 
            'created_at' => '2018-12-01 22:18:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 17, 'cep' => '96060-200',
            'rua' => 'Doutor Cardoso Fontes',
            'numero' => '678', 'bairro' => 'Três Vendas',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98122-3435', 
            'user_id' => 17, 
            'created_at' => '2018-12-01 22:18:01'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 18, 
            'user_id' => 18, 
            'created_at' => '2018-12-02 15:22:50'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 19, 
            'user_id' => 19, 
            'created_at' => '2018-12-02 15:24:50'        
        ]);
        DB::table('contatosusers')->insert([
            'id' => 20, 
            'user_id' => 20, 
            'created_at' => '2018-12-02 15:26:50'        
        ]);
    }
}
