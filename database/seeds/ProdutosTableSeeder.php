<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Pão',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:31:25',
            'updated_at' => '2018-12-01 18:31:25'
        ]);
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Maionese',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:31:34',
            'updated_at' => '2018-12-01 18:31:35'
        ]);
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Hambúrguer de Carne',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:32:40',
            'updated_at' => '2018-12-01 18:32:43'
        ]);
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Hambúrguer de Frango',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);
    }
}
