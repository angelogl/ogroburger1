<?php

use Illuminate\Database\Seeder;

class MercadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mercados')->insert([
            'razaosocial' => 'Peruzzo Supermercados',  
            'cnpj' => '87.397.865/0009-79',          
            'telefone' => '(53) 3227-4022',
            'site' => 'https://www.peruzzo.com.br',
            'foto' => 'imagens/mercado.png',
            'ativo' => 0, 'created_at' => '2018-11-29 22:37:25',
            'updated_at' => '2018-11-29 22:37:25'
        ]);
        DB::table('mercados')->insert([
            'razaosocial' => 'Macro Atacado Krolow',  
            'cnpj' => '01.060.735/0001-73',
            'email' => 'contato@macroatacadokrolow.com.br',
            'telefone' => '(53) 3223-1869',
            'site' => 'http://www.macroatacadokrolow.com.br',
            'foto' => 'imagens/mercado.png',
            'ativo' => 0, 'created_at' => '2018-11-30 12:53:25',
            'updated_at' => '2018-11-30 12:53:25'
        ]);
        DB::table('mercados')->insert([
            'razaosocial' => 'Supermercado Guanabara',  
            'cnpj' => '94.846.755/0007-40',
            'email' => 'cliente@smguanabara.com.br',
            'telefone' => '(53) 2125-22222',
            'site' => 'http://www.macroatacadokrolow.com.br',
            'foto' => 'imagens/mercado.png',
            'ativo' => 0, 'created_at' => '2018-11-30 12:53:57',
            'updated_at' => '2018-11-30 12:53:57'
        ]);
        DB::table('mercados')->insert([
            'razaosocial' => 'Maxxi Atacado',  
            'cnpj' => '93.209.765/0294-41',
            'telefone' => '(53) 2128-7105',
            'site' => 'https://www.maxxiatacado.com.br',
            'foto' => 'imagens/maxxi.png',
            'ativo' => 0, 'created_at' => '2018-11-30 13:06:05',
            'updated_at' => '2018-11-30 13:06:05'
        ]);
        DB::table('mercados')->insert([
            'razaosocial' => 'Atacadão',  
            'cnpj' => '75.315.333/0122-04',
            'telefone' => '(53) 3302-4626',
            'site' => 'https://www.atacadao.com.br/ofertas/pelotas',
            'foto' => 'imagens/atacadao.png',
            'ativo' => 0, 'created_at' => '2018-11-30 13:11:10',
            'updated_at' => '2018-11-30 13:10:10'
        ]);
        DB::table('mercados')->insert([
            'razaosocial' => 'Macro Atacado Treichel',  
            'cnpj' => '03.204.565/0001-89',
            'email' => 'sac@atacadotreichel.com.br',
            'telefone' => '(53) 3284-8878',
            'celular' => '(53) 98401-2523',
            'site' => 'https://delivery.atacadotreichel.com.br',
            'foto' => 'imagens/treichel.png',
            'ativo' => 0, 'created_at' => '2018-11-30 13:15:20',
            'updated_at' => '2018-11-30 13:15:20'
        ]);
    }
}
