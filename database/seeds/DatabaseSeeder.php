<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ContatosusersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(ContatosadminsTableSeeder::class);
        $this->call(MercadosTableSeeder::class);
        $this->call(ProdutosTableSeeder::class);
        $this->call(ComprasTableSeeder::class);
    }
}
