<?php

use Illuminate\Database\Seeder;

class ComprasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compras')->insert([
            'mercado_id' => 1,
            'produto_id' => 2,
            'quantidade' => 3,
            'custounitario' => 1.50,
            'total' => 4.50,
            'created_at' => '2018-12-01 21:59:25'
        ]);
    }
}
