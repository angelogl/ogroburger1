<?php

use Illuminate\Database\Seeder;

class ContatosadminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contatosadmins')->insert([
            'id' => 1, 'cep' => '96030-390',
            'rua' => 'Marechal Setembrino de Carvalho',
            'numero' => '104', 'bairro' => 'Fragata',
            'cidade' => 'Pelotas', 'uf' => 'RS',
            'celular1' => '(53) 98143-1723', 'celular2' => '(53) 98445-7095',
            'facebook' => 'https://pt-br.facebook.com/rafael.souzacalearo',
            'twitter' => 'https://twitter.com/@rafatheonly',
            'instagram' => 'https://www.instagram.com/rafaelsouzacalearo',
            'admin_id' => 1, 'created_at' => '2018-10-31 23:34:23',
            'updated_at' => '2018-10-31 23:35:46'
        ]);
    }
}
