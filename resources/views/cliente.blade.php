@extends('layouts.dashboard', ["current" => "clientes"])
@section('conteudo')
<!-- PARTE DO CONTEUDO EM SI -->         
<div class="page-wrapper">
<!-- BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" --> 
<div class="page-breadcrumb">
   <div class="row align-items-center">
      <div class="col-12">
         <h4 class="page-title">CLIENTE</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">                 
                     <a href="\admin">Home (Dashboard)</a>
                  </li>
                  <li class="breadcrumb-item">                 
                     <a href="\admin\clientes">Clientes</a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Novo</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
<!-- CONTEUDO FLUIDO  --> 
<div class="container-fluid">
   <!-- LINHA -->
   <div class="row">
      <!-- COLUNA DO FORMULARIO DE CRIACAO DE UM NOVO CLIENTE -->
      <div class="col-lg-12 col-xlg-12 col-md-12">
         <div class="card">
            <div class="card-header titulo-card">
               <h5>CADASTRO DE NOVO CLIENTE</h5>
               <div class="form-requerido">
                  <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
               </div>
            </div>
            <div class="card-body">
               <form method="POST" novalidate enctype="multipart/form-data"              
                  action="/admin/clientes/novo"              
                  class="form-horizontal form-material">
                  @csrf
                  <div class="form-row">
                     <div class="form-group col-md-6">              
                        <label for="name"><strong>Nome completo</strong>
                        <span class="form-requerido">*</span>
                        <span class="aviso-senha">· ex.: Nome Sobrenome</span></label>
                        <input id="name" type="text" 
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-sm"
                           name="name"
                           placeholder="Digite o nome completo" />
                        @if ($errors->has('name'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>                                    
                        </span>
                        @endif                   
                     </div>
                     <div class="form-group col-md-6">   
                        <label for="email"><strong>E-mail</strong> 
                        <span class="form-requerido">*</span></label>
                        <input id="email" type="email" 
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-sm"
                           name="email"
                           placeholder="Digite o e-mail" />
                        @if ($errors->has('email'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="password"><strong>Senha</strong> <span class="form-requerido">*</span>
                        <span class="aviso-senha">· 6 digitos ou maior</span></label>
                        <input id="password" type="password" 
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" 
                           name="password" required placeholder="Digite a senha">
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label for="password-confirm"><strong>Confirme a senha</strong> <span class="form-requerido">*</span></label>                           
                        <input id="password-confirm" type="password" name="password_confirmation"  
                           class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} form-control-sm" 
                           required placeholder="Digite a senha novamente"> 
                        @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>                                  
                        </span>
                        @endif                          
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-4">
                        <label for="celular1"><strong>Celular/WhatsApp</strong> 
                        <span class="form-requerido">*</span></label>
                        <input id="celular1" type="text" 
                           class="form-control{{ $errors->has('celular1') ? ' is-invalid' : '' }} form-control-sm" 
                           name="celular1"
                           placeholder="Digite o celular/WhatsApp" />
                        @if ($errors->has('celular1'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('celular1') }}</strong>                                    
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-4">
                        <label for="celular2"><strong>Celular 2</strong></label>
                        <input id="celular2" type="text" 
                           class="form-control form-control-sm" 
                           name="celular2" 
                           placeholder="Digite outro celular"/>                    
                     </div>
                     <div class="form-group col-md-4">
                        <label for="residencial">
                        <strong>Telefone residencial</strong>
                        </label>
                        <input id="residencial" type="text" 
                           class="form-control form-control-sm" 
                           name="residencial" 
                           placeholder="Digite o tel. residencial" />                                       
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-3">
                        <label for="cep"><strong>CEP</strong>
                        <span class="form-requerido">*</span></label>                    
                        <input id="cep" type="text" 
                           class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }} form-control-sm" 
                           name="cep" 
                           placeholder="Digite o CEP"/>
                        @if ($errors->has('cep'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cep') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-6">
                        <label for="rua"><strong>Rua</strong>
                        <span class="form-requerido">*</span></label>                    
                        <input id="rua" type="text" 
                           class="form-control{{ $errors->has('rua') ? ' is-invalid' : '' }} form-control-sm" 
                           name="rua" placeholder="Digite a rua" />
                        @if ($errors->has('rua'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('rua') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-3">
                        <label for="numero"><strong>Número</strong>
                        <span class="form-requerido">*</span></label>                    
                        <input id="numero" type="number" 
                           class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }} form-control-sm" 
                           name="numero" 
                           placeholder="Digite o n.°" />
                        @if ($errors->has('numero'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('numero') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-6">   
                        <label for="complemento"><strong>Complemento</strong></label>
                        <input id="complemento" type="text" 
                           class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }} form-control-sm"
                           name="complemento" 
                           placeholder="Digite o complemento" />
                        @if ($errors->has('complemento'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('complemento') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-6">   
                        <label for="bairro"><strong>Bairro</strong>
                        <span class="form-requerido">*</span></label>
                        <input id="bairro" type="text" 
                           class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }} form-control-sm"
                           name="bairro"
                           placeholder="Digite o bairro" />
                        @if ($errors->has('bairro'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('bairro') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-5">
                        <label for="cidade"><strong>Cidade</strong>
                        <span class="form-requerido">*</span></label>                    
                        <input id="cidade" type="text" 
                           class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }} form-control-sm" 
                           name="cidade" 
                           placeholder="Digite a cidade" />
                        @if ($errors->has('cidade'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cidade') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-2">
                        <label for="uf"><strong>UF</strong>
                        <span class="form-requerido">*</span></label>                    
                        <input id="uf" type="text" 
                           class="form-control{{ $errors->has('uf') ? ' is-invalid' : '' }} form-control-sm" 
                           name="uf" 
                           placeholder="Digite a UF" />
                        @if ($errors->has('uf'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('uf') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-5">              
                        <label for="foto"><strong>Foto (Perfil)</strong>
                        <span class="aviso-senha">· ideal 150 x 150 pixels</span></label>
                        <input id="foto" type="file" name="foto"
                           class="form-control-file form-control-sm" />                                      
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-4">              
                        <label for="facebook"><strong>Facebook</strong></label>
                        <input id="facebook" type="text" 
                           class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }} form-control-sm"
                           name="facebook" 
                           placeholder="Digite a URL do Facebook" />
                        @if ($errors->has('facebook'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('facebook') }}</strong>                                    
                        </span>
                        @endif                   
                     </div>
                     <div class="form-group col-md-4">              
                        <label for="twitter"><strong>Twitter</strong></label>
                        <input id="twitter" type="text" 
                           class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }} form-control-sm"
                           name="twitter" 
                           placeholder="Digite a URL do Twitter" />
                        @if ($errors->has('twitter'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('twitter') }}</strong>                                    
                        </span>
                        @endif                   
                     </div>
                     <div class="form-group col-md-4">              
                        <label for="instagram"><strong>Instagram</strong></label>
                        <input id="instagram" type="text" 
                           class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }} form-control-sm"
                           name="instagram" 
                           placeholder="Digite a URL do Instagram" />
                        @if ($errors->has('instagram'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('instagram') }}</strong>                                    
                        </span>
                        @endif                   
                     </div>
                  </div>
                  <hr>
                  <div class="text-right">
                     <button class="btn btn-success btn-sm btn-espaco" type="submit">
                     <i class="mdi mdi-content-save"></i> CADASTRAR CLIENTE</button> 
                     <button class="btn btn-primary btn-sm btn-espaco" type="reset">
                     <i class="mdi mdi-broom"></i> LIMPAR</button>   
                     <a href="/admin/clientes" 
                        class="btn btn-secondary btn-sm btn-espaco">
                     <i class="mdi mdi-close"></i> CANCELAR</a>                                                         
                  </div>
               </form>
            </div>
         </div>
      </div>     
      <!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->    
   </div>
   <!-- FIM DA LINHA -->   
</div>
<!-- FIM DO CONTEUDO FLUIDO  --> 
<!-- AQUI TERIA Q TER UMA </div> PRA FECHAR A PARTE DO CONTEUDO EM SI
   MAS ELA ESTA NA PARTE DO LAYOUT DO DASHBOARD -->  
@endsection