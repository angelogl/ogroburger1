@extends('layouts.dashboard', ["current" => "compras"])
@section('conteudo')
<!-- PARTE DO CONTEUDO EM SI -->
<div class="page-wrapper">
    <!-- BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-12">
                <h4 class="page-title">COMPRA</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="\admin">Home (Dashboard)</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Compra</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
    <!-- CONTEUDO FLUIDO  -->
    <div class="container-fluid">
        <!-- LINHA -->
        <div class="row">
            <!-- COLUNA DO FORMULARIO DE CRIACAO DE UM NOVO CLIENTE -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-header titulo-card">
                        <h5>CADASTRO DE NOVA COMPRA</h5>
                        <div class="form-requerido">
                            <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="/admin/compras/nova" class="form-horizontal form-material">
                            @csrf
                            <h6 class="card-title mt-4"><strong>INFORMAÇÕES DA COMPRA</strong></h6>
                            <div class="form-row borda mb-5">
                                <div class="form-group col-md-4">
                                    <label for="fornecedor"><strong>Fornecedor</strong>
                                        <span class="form-requerido">*</span></label>
                                    <select class="form-control" name="fornecedor">
                                        @foreach($fornecedores as $f)
                                        <option value="{{$f->id}}">{{$f->razaosocial}}</option>
                                        @endforeach
                                    </select>                                    
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="nf"><strong>Núm. NF</strong>
                                        <span class="form-requerido">*</span></label>
                                    <input id="nf" type="text" class="form-control{{ $errors->has('nf') ? ' is-invalid' : '' }}"
                                        name="nf" placeholder="Digite o núm. da NF" />
                                    @if ($errors->has('nf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nf') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="data"><strong>Data</strong>
                                        <span class="form-requerido">*</span></label>
                                    <input type="date" id="data"  
                                    class="form-control{{ $errors->has('data') ? ' is-invalid' : '' }}"
                                    name="data" />
                                    @if ($errors->has('data'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('data') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <h6 class="card-title"><strong>PRODUTO(S) DA COMPRA</strong></h6>
                            <div class="form-row borda" id="novo">
                                <div class="form-group col-md-12 text-right">
                                   <button type="button" id="adicionar" 
                                   class="btn btn-info btn-sm" disabled>ADICIONAR PRODUTO</button>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="produto"><strong>Produto</strong>
                                        <span class="form-requerido">*</span></label>
                                    <select class="form-control" name="produto0">
                                        @foreach($produtos as $p)
                                        <option value="{{$p->id}}">{{$p->nome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="quantidade"><strong>Quantidade</strong>
                                        <span class="form-requerido">*</span></label>
                                    <input type="number" id="quantidade0" 
                                    class="form-control quantidade" name="quantidade0" />
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="custo"><strong>Custo un.</strong>
                                        <span class="form-requerido">*</span></label>
                                    <input type="text" id="custo0" class="form-control" name="custo0" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="total"><strong>Total</strong>
                                        <span class="form-requerido">*</span></label>
                                    <input type="text" id="total0" class="form-control" 
                                    name="total0" disabled />
                                </div>                                
                            </div>
                            <div class="text-right mt-4">
                               <h4><strong>TOTAL DA COMPRA: <span id="totalCompra"></span></strong></h4>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button class="btn btn-success btn-sm btn-espaco" type="submit">
                                    <i class="mdi mdi-content-save"></i> CADASTRAR COMPRA</button>
                                <button class="btn btn-primary btn-sm btn-espaco" type="reset">
                                    <i class="mdi mdi-broom"></i> LIMPAR</button>
                                <a href="/admin" class="btn btn-secondary btn-sm btn-espaco">
                                    <i class="mdi mdi-close"></i> CANCELAR</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->
        </div>
        <!-- FIM DA LINHA -->
    </div>

<!-- FIM DO CONTEUDO FLUIDO  -->
<!-- AQUI TERIA Q TER UMA </div> PRA FECHAR A PARTE DO CONTEUDO EM SI
   MAS ELA ESTA NA PARTE DO LAYOUT DO DASHBOARD -->
@endsection
@if (session('OK'))
      <div class="alert alerta-sucesso alert-dismissible" role="alert">
         <i class="fas fa-check-circle"></i>{{ session('OK') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
@endif
@section('js')
<script type="text/javascript">
$(document).ready(function () {

var i = 0;
var stringCusto = '#custo' + i;
var stringQuantidade = '#quantidade' + i;
var stringTotal = '#total' + i;
var totalCompra = 0;

//$(document).on('blur', stringCusto, function () {    
$(stringCusto).blur(function () {
    var resp = $(stringCusto).val().split(",");
    var valor = resp[0] + "." + resp[1];
    var total = valor * $(stringQuantidade).val();
    if (isNaN(total)) {
        $(stringTotal).val(0);
    } else {
        $(stringTotal).val(total.toFixed(2).replace('.', ','));
        $('#adicionar').prop("disabled", false);
        totalCompra = totalCompra + total;
        $('#totalCompra').html('R$ ' + totalCompra.toFixed(2).replace('.', ','));
    }
});

$("#adicionar").click(function (e) {
    e.preventDefault();
    i++;
    $('#adicionar').prop("disabled", true);
    var linha = '<div class="form-group col-md-4">' +
        '<label for="produto"><strong>Produto</strong> ' +
        '<span class="form-requerido">*</span></label>' +
        '<select name="produto' + i + '" class="form-control">@foreach($produtos as $p)' +
        '<option value="{{$p->id}}">{{$p->nome}}</option>@endforeach' +
        '</select></div><div class="form-group col-md-2">' +
        '<label for="quantidade"><strong>Quantidade</strong> ' +
        '<span class="form-requerido">*</span></label>' +
        '<input id="quantidade' + i + '" type="number" class="form-control"' +
        ' name="quantidade' + i + '" /></div><div class="form-group col-md-2">' +
        '<label for="custo"><strong>Custo un.</strong> ' +
        '<span class="form-requerido">*</span></label><input id="custo' + i +
        '" type="text" class="form-control" name="custo' + i + '" /></div>' +
        '<div class="form-group col-md-2"><label for="total">' +
        '<strong>Total</strong> <span class="form-requerido">*</span>' +
        '</label><input id="total' + i + '" type="text" class="form-control"' +
        ' name="total' + i + '" disabled /></div><div class="form-group col-md-2">' +
        '<label><strong>Ação</strong></label><div><button type="button"' +
        'class="btn btn-danger btn-block" id="remover">REMOVER</button></div></div>';
    $('#novo').append(linha);
    var stringCusto = '#custo' + i;
    var stringQuantidade = '#quantidade' + i;
    var stringTotal = '#total' + i;
    $(stringCusto).blur(function () {
        var resp = $(stringCusto).val().split(",");
        var valor = resp[0] + "." + resp[1];
        var total = valor * $(stringQuantidade).val();
        if (isNaN(total)) {
            $(stringTotal).val(0);
        } else {
            $(stringTotal).val(total.toFixed(2).replace('.', ','));
            $('#adicionar').prop("disabled", false);
            totalCompra = totalCompra + total;
            $('#totalCompra').html('R$ ' + totalCompra.toFixed(2).replace('.', ','));
        }
    });
});



});
</script>
@endsection
