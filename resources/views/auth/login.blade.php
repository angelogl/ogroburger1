<!doctype html>
<html>
   <head>
      <title>Login - Ogro Burger - Hamburgueria</title>
      <meta charset="UTF-8">
      <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <!-- Favicon (ícone na aba do navegador) -->
      <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon"/>
      <link rel="shortcut icon" type="image/x-icon" href="{!! asset('favicon.ico') !!}" />
      <!-- Estilos (Bootstrap e personalizado) -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">      
   </head>
   <body>
      <div id="root" class="fundo-login">
         <main class="cr-app">
            <div class="cr-content container-fluid">
               <div class="row form-center">
                  <div class="col-md-5 col-lg-3">
                     <div class="card card-body">
                        <form method="POST" action="{{ route('login') }}" novalidate>
                           @csrf
                           <div class="text-center pb-4">
                              <a href="/">
                              <img src="{!! asset('img/logo-login.png') !!}" 
                                 alt="logo" class="margem-baixo-logo-cad-login">
                              </a>
                              <h5><strong>OLÁ, OGRO(A)</strong></h5>
                           </div>
                           <div class="form-group">
                              <label for="email">E-mail</label>
                              <input id="email" type="email" 
                                 class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-sm" 
                                 name="email" value="{{ old('email') }}" required autofocus 
                                 placeholder="Digite seu e-mail">
                              @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                 <!--<strong>{{ $errors->first('email') }}</strong>-->
                                 <strong>O e-mail é obrigatório!</strong>
                              </span>
                              @endif
                           </div>
                           <div class="form-group">
                              <label for="password">Senha</label>
                              <input id="password" type="password" 
                                 class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" 
                                 name="password" required placeholder="Digite sua senha">
                              @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                 <!--<strong>{{ $errors->first('password') }}</strong>-->
                                 <strong>A senha é obrigatória!</strong>
                              </span>
                              @endif
                           </div>
                           <div class="form-check">
                              <!--<label class="form-check-label" for="remember">-->
                              <label for="remember">
                              <input class="form-check-input" type="checkbox" 
                              name="remember" id="remember" 
                              {{ old('remember') ? 'checked' : '' }}> Permanecer conectado?</label>
                           </div>
                           <button type="submit" 
                           class="border-0 btn btn-primary btn-block">ENTRAR</button>
                           <div class="text-center pt-1">
                              <small><a href="#">ESQUECEU A SUA SENHA?</a></small>
                              <hr>
                              <small>
                              <strong>OU</strong>
                              <a href="/register"><strong>CADASTRE-SE</strong></a> |                           
                              <a href="/" class="btn btn-outline-primary btn-sm">
                              <small><i class="fas fa-home"></i> VOLTAR</small></a>
                              </small>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </main>
      </div>
   </body>
</html>