<?php

namespace App\Http\Controllers;

use App\Contatosuser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $usuario = Auth::user();
            $contato = Contatosuser::find($usuario->id);
            if (isset($contato)) {
                return view('home');
            } else {
                $contato = new Contatosuser();
                $contato->user_id = $usuario->id;
                $contato->created_at = new \DateTime();
                $contato->save();
                return view('home');
            }
        }
    }

    public function perfil($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            return view('perfil', compact('usuario'));
        }
    }

    /**
     * METODO QUE ALTERA OS DADOS DO USUARIO (CLIENTE) E SEUS CONTATOS
     * @param  \Illuminate\Http\Request  $request SAO OS DADOS DO FORM
     * @param  int  $id E O NUM. CORRESPONDENTE AO USUARIO A TER OS DADOS ALTERADOS
     * @return \Illuminate\Http\Response E A RESPOSTA ENVIADA P/ A VIEW
     */
    public function update(Request $request, $id)
    {
        $request->validate(['name' => 'required|string|min:3|max:30',
            'email' => 'required|string|email', 'celular1' => 'required',
            'cep' => 'required', 'rua' => 'required|string|max:30',
            'numero' => 'required|max:10', 'complemento' => 'max:30',
            'bairro' => 'required|string|max:30',
            'cidade' => 'required|string|max:30', 'uf' => 'required'],
            ['required' => 'O campo é obrigatório!',
                'min' => 'O campo deve ter mais que 2 letras!',
                'max' => 'O campo deve ser menor que 30 letras!',
                'email.email' => 'O e-mail não é válido!']);
        $usuario = User::find($id);
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $foto = $request->file('foto');
        if (isset($foto)) {
            if ($usuario->foto === "imagens/perfil.png") {
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            } else {
                Storage::disk('public')->delete($usuario->foto);
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            }
        }
        $usuario->save();
        $contatos = Contatosuser::where('user_id', $id)->get()->first();
        $contatos->cep = $request->input('cep');
        $contatos->rua = $request->input('rua');
        $contatos->numero = $request->input('numero');
        $contatos->complemento = $request->input('complemento') == null ? "—" : trim($request->input('complemento'));
        $contatos->bairro = $request->input('bairro');
        $contatos->cidade = $request->input('cidade');
        $contatos->uf = $request->input('uf');
        $contatos->celular1 = $request->input('celular1');
        $contatos->celular2 = $request->input('celular2') == null ? "—" : $request->input('celular2');
        $contatos->residencial = $request->input('residencial') == null ? "—" : $request->input('residencial');
        $contatos->facebook = $request->input('facebook') == null ? "—" : trim($request->input('facebook'));
        $contatos->twitter = $request->input('twitter') == null ? "—" : trim($request->input('twitter'));
        $contatos->instagram = $request->input('instagram') == null ? "—" : trim($request->input('instagram'));
        $contatos->save();
        $request->session()->flash('OK', ' DADOS ATUALIZADOS COM SUCESSO!');
        return redirect('home/perfil/' . $id);
    }
}
