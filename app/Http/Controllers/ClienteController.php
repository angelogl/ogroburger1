<?php

namespace App\Http\Controllers;

use App\Contatosuser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $clientes = User::where('ativo', 0)->orderBy('name')->paginate(5);
        if ($request->ajax()) {
            return view('componentes.tabela-clientes', array('clientes' => $clientes))->render();
        }
        return view('clientes', compact('clientes'));
    }

    public function desativados()
    {
        $clientes = User::where('ativo', 1)->orderBy('name')->get();
        return json_encode($clientes);
    }

    public function controles()
    {
        $controles['totalClientes'] = User::all()->count();        
        $controles['dataUltimo'] = date("d/m/Y", strtotime(User::max('created_at')));
        $controles['totalDesativados'] = User::where('ativo', 1)->count();      
        $controles['dataDesativados'] = date("d/m/Y", strtotime(User::where('ativo', 1)->max('updated_at')));
        $controles['semEndereco'] = Contatosuser::where('rua', "—")->count();
        $controles['semFotos'] = User::where('foto', 'imagens/perfil.png')->count();      
        $controles['dataFotos'] = date("d/m/Y", strtotime(User::where('foto', 'imagens/perfil.png')->max('created_at')));
        return json_encode($controles);
    }

    public function desativar($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            $usuario->ativo = 1;
            $usuario->save();
        }
        return redirect('admin/clientes');
    }

    public function ativar($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            $usuario->ativo = 0;
            $usuario->save();
        }
        return redirect('admin/clientes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|string|min:3|max:30',
            'email' => 'required|string|email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required', 'celular1' => 'required',
            'cep' => 'required', 'rua' => 'required|string|max:30',
            'numero' => 'required|max:10', 'complemento' => 'max:30',
            'bairro' => 'required|string|max:30',
            'cidade' => 'required|string|max:30', 'uf' => 'required'],
            ['required' => 'O campo é obrigatório!',
                'min' => 'O campo deve ter mais que 2 letras!',
                'max' => 'O campo deve ser menor que 30 letras!',
                'email.email' => 'O e-mail não é válido!',
                'password.min' => 'O campo deve ser maior que 5 digitos!']);
        $id = User::all()->count() + 1;
        $usuario = new User();
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $usuario->password = Hash::make($request->input('password'));
        $foto = $request->file('foto');
        if (isset($foto)) {
            $usuario->foto = $request->file('foto')->store('imagens', 'public');
        }
        $usuario->save();
        $contatos = new Contatosuser();
        $contatos->cep = $request->input('cep');
        $contatos->rua = $request->input('rua');
        $contatos->numero = $request->input('numero');
        $contatos->complemento = $request->input('complemento') == null ? "—" : trim($request->input('complemento'));
        $contatos->bairro = $request->input('bairro');
        $contatos->cidade = $request->input('cidade');
        $contatos->uf = $request->input('uf');
        $contatos->celular1 = $request->input('celular1');
        $contatos->celular2 = $request->input('celular2') == null ? "—" : $request->input('celular2');
        $contatos->residencial = $request->input('residencial') == null ? "—" : $request->input('residencial');
        $contatos->facebook = $request->input('facebook') == null ? "—" : trim($request->input('facebook'));
        $contatos->twitter = $request->input('twitter') == null ? "—" : trim($request->input('twitter'));
        $contatos->instagram = $request->input('instagram') == null ? "—" : trim($request->input('instagram'));
        $contatos->user_id = $id;
        $contatos->save();
        $request->session()->flash('OK', ' CLIENTE CADASTRADO COM SUCESSO!');
        return redirect('admin/clientes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            return view('cliente-editar', compact('usuario'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['name' => 'required|string|min:3|max:30',
            'email' => 'required|string|email', 'celular1' => 'required',
            'cep' => 'required', 'rua' => 'required|string|max:30',
            'numero' => 'required|max:10', 'complemento' => 'max:30',
            'bairro' => 'required|string|max:30',
            'cidade' => 'required|string|max:30', 'uf' => 'required'],
            ['required' => 'O campo é obrigatório!',
                'min' => 'O campo deve ter mais que 2 letras!',
                'max' => 'O campo deve ser menor que 30 letras!',
                'email.email' => 'O e-mail não é válido!']);
        $usuario = User::find($id);
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $foto = $request->file('foto');
        if (isset($foto)) {
            if ($usuario->foto === "imagens/perfil.png") {
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            } else {
                Storage::disk('public')->delete($usuario->foto);
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            }
        }
        $usuario->save();
        $contatos = Contatosuser::where('user_id', $id)->get()->first();
        $contatos->cep = $request->input('cep');
        $contatos->rua = $request->input('rua');
        $contatos->numero = $request->input('numero');
        $contatos->complemento = $request->input('complemento') == null ? "—" : trim($request->input('complemento'));
        $contatos->bairro = $request->input('bairro');
        $contatos->cidade = $request->input('cidade');
        $contatos->uf = $request->input('uf');
        $contatos->celular1 = $request->input('celular1');
        $contatos->celular2 = $request->input('celular2') == null ? "—" : $request->input('celular2');
        $contatos->residencial = $request->input('residencial') == null ? "—" : $request->input('residencial');
        $contatos->facebook = $request->input('facebook') == null ? "—" : trim($request->input('facebook'));
        $contatos->twitter = $request->input('twitter') == null ? "—" : trim($request->input('twitter'));
        $contatos->instagram = $request->input('instagram') == null ? "—" : trim($request->input('instagram'));
        $contatos->save();
        $request->session()->flash('OK', ' DADOS ATUALIZADOS COM SUCESSO!');
        return redirect('admin/clientes/editar/' . $id);
    }

    public function busca(Request $request)
    {
        $request->validate(['aPesquisar' => 'required']);
        $palavra = trim($request->input('aPesquisar'));
        return view('cliente-busca', compact('palavra'));
    }

    public function procurar(Request $request)
    {
        $request->validate(['p' => 'required']);
        $palavra = $request->get('p');
        $clientes = User::with(['contato'])->where('name', 'like', '%' . $palavra . '%')->orderBy('name')->get();
        return json_encode($clientes);
    }
}
