<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    /**
     * CONSTRUTOR Q INICIALIZA A PERMISSAO P/ SOMENTE
     * OS ADMINS USAREM AS FUNCOES DESSA CLASSE
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    // FUNCAO Q CHAMA A VIEW Q CONTEM O FORMULARIO DE ACESSO P/ OS ADMINS
    public function index()
    {
        return view("auth.admin-login");
    }

    /**
     * FUNCAO QUE VALIDA O LOGIN DO ADMIN
     * @param \Illuminate\Http\Request  $request SAO OS DADOS DO FORM DE ACESSO
     * @return view QUE MOSTRA QUE O LOGIN FOI ACEITO, OU
     * @return redirect PARA O FORM DE LOGIN NOVAMENTE CASO O LOGIN N OCORRA BEM
     */
    public function login(Request $request)
    {
        // VALIDA OS DADOS VINDO DO FORMULARIO DE LOGIN
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // CRIA AS CREDENCIAIS DE ACESSO
        $credentials = ['email' => $request->email,
            'password' => $request->password];

        // GUARDA O ESTADO DA AUTENTICACAO: true OU false
        $authOk = Auth::guard('admin')->attempt($credentials, $request->remember);

        // SE AS $credentials ESTIVEREM CORRETAS, OU SEJA, true
        if ($authOk) {
            /**
             * QUANDO O USUARIO DESLOGADO TENTA ACESSAR UMA PAG. (VIEW) Q ESTA SOB
             * PROTECAO DO middleware('guest:admin') O LARAVEL REDIRECIONARA
             * O USUARIO P/ O LOGIN, E ESSA PAGINA (VIEW) SERA MANTIDA CASO
             * O LOGIN SEJA EFETUADO CORRETAMENTE, A MESMA SERA RETORNADA. SE N
             * HOUVER NENHUM PAG. (VIEW) REQUISITADA ANTES DO LOGIN, O LARAVEL
             * REDIRECIONARA O USUARIO P/ A PAG. HOME DO DASHBOARD, OU SEJA, P/
             * ROTA PADRAO PASSADA AO METODO intended
             */
            return redirect()->intended(route('admin.dashboard'));
        }
        /**
         * SE N, REDIRECIONA NOVAMENTE P/ A VIEW DE LOGIN, PASSANDO OS
         * PARAMETROS DO REQUEST P/ O FORMULARIO CONTIDO NESSA VIEW
         */
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
